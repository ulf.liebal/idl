function mymodel = iDL_aux_Dat2Mod(DataBucket)
% The function integrates MDVs, standard deviations, external rates, the
% experimental set-up with a metabolic model and saves the model in the
% address of the MDV data. This function calls INCA, therefore INCA needs
% to be in the search path.
%
% Input:
%       DataBucket: structure, from iDL_main function
%       MDV-file: Excel sheet with MDVs, standard deviation, and time
%                 vector. The file optionaly contains external rate
%                 measurements.
%       Model-file: m-file function that generates an INCA reaction class
%                 of the metabolic pathway.
%       Experiment-file: m-file function that generates INCA msdata and
%                 tracer classes    
%
% Output:
%       mymodel: structure, INCA model
%       mat-file: INCA model as MATLAB variable

curAddr = pwd;
INCA_loaded=exist('model'); % test whether INCA is in the search path, result should be '6' as p-file.
if INCA_loaded ~=6
    msgbox('Load INCA into Matlab search path.','INCA not found')
    return
end

if ~isfield(DataBucket,'DataStandardDev')
    msgbox('Load Data and StdDev into iDL.','Data missing')
    return
end
if ~isfield(DataBucket,'ModelSource')
    msgbox('Load INCA Model into iDL.','Model missing')
    return
end

Dec_INST = questdlg('Do you want to perform INST-MFA?','INST-MFA Decision','Yes','No','No');
switch Dec_INST
    case 'No'
        uiwait(msgbox('Select the measurement values in the following sheet view.','Select MDV Data'))
        myallMDVs = iDL_aux_LoadXlsRange(DataBucket.DataCorrected);
    case 'Yes'
        uiwait(msgbox('Select the measurement values in the following sheet view.','Select MDV Data'))
        myallMDVs = iDL_aux_LoadXlsRange(DataBucket.DataCorrected);
        % First row is assumed to represent sampling point time vector.
        uiwait(msgbox('Select the time vector row in the following sheet view.','Select Sample Times'))
        INSTtime = iDL_aux_LoadXlsRange(DataBucket.DataCorrected);
end
if ischar(DataBucket.DataStandardDev)
    uiwait(msgbox('Select the standard deviation values in the following sheet view.','Select Stdev Data'))
    myallStds = iDL_aux_LoadXlsRange(DataBucket.DataStandardDev);
else
    % Determining standard deviation of measurements
    % GC/MS standard error ranges linearly, taken from INCA, E. coli
    % example
    x0 = 0.005; e0 = 0.003; x1 = 0.25; e1 = 0.05; 
    myallStds = max(min((e1-e0)/(x1-x0)*(myallMDVs-x1)+e1,e1),e0);
end

% Decision whether to include metabolite pool sizes
% The sheet containing the pool concentrations need to be named 'Pools'.
% Column A needs the metabolite identifier as in the model
% Column B needs to contain the concentration value
% Column C needs to contain the standard deviation
Dec_Pool = questdlg('Do you want to load pool sizes from the MDV-data file?','Pool Decision','Yes','No','No');


%%%%%%%%%%%%%%%%%
% Generating model
%%%%%%%%%%%%%%%%%
[ModelPath,ModelFile,~] = fileparts(DataBucket.ModelSource);
cd(ModelPath)
mymodel = feval(ModelFile);
% In the following the MS metabolite file is loaded that defines the measured metabolites. 
[FileName,FilePath]= uigetfile('*.m','Select experiment description file');
ExPath = fullfile(FilePath, FileName);
[MSPath,MSFile,~] = fileparts(ExPath);
cd(MSPath)
x = feval(MSFile);
% adding more experiments
% Dec_ExptNum = questdlg('Do you want to add multiple experiments?','Number of experiments','No','Yes','No');
%ExptNum = 1;
%switch Dec_ExptNum
%    case 'Yes'
%    Expt_answer = inputdlg('How many experiments?','Experiment number',[1,20]);
%    ExptNum = str2double(Expt_answer{1});
%    for i1 = 2:ExptNum
%        x(i1) = x(1);
%        x(i1).id = {['Expt #',num2str(i1)]};
%    end
%     case 'Yes: change tracer/analytes'
%     Expt_answer = inputdlg('How many experiments?','Experiment number',[1,20]);
%     ExptNum = str2double(Expt_answer{1});
%     for i1 = 2:ExptNum
%         Dec_AddAnalyte = questdlg('Do you want to add multiple experiments?','Number of experiments','No','Yes: identical conditions','Yes: change tracer/analytes','No');
%         x(i1) = x(1);
%         x(i1).id = {['Expt #',num2str(i1)]};
%     end    
%end

mymodel.expts = x;
% The INCA options are added to the model. 
iDL_bin = fullfile(DataBucket.iDL_Address,'bin');
cd(iDL_bin)
%iDL_INCAoptions = fullfile(iDL_template,'INCA_options');
mymodel = iDL_aux_INCAoptions(mymodel);
if strcmp(Dec_INST,'Yes')
    mymodel.options.int_tspan = INSTtime;
    mymodel.options.sim_ss = false;
end

%%%%%%%%%%%%%%%%%
% Putting measured data in each metabolite
%%%%%%%%%%%%%%%%%
% To be able to put data into value blocks, I first enter simulated data
% with sim2mod
mymodel.rates.flx.val = rand(size(mymodel.rates.flx,2),1);
mymodel.rates.flx.val = mod2stoich(mymodel)';
% simulate measurements
s = simulate(mymodel);
mymodel=sim2mod(mymodel,s);

numbmet = size(mymodel.expts(1).data_ms,2);    
for i1 = 1:numbmet
    % number of measured masses to be extracted
    numbmass = size(str2num(cell2mat(mymodel.expts.data_ms(i1).atoms)),2);
%    for i2 = 1:ExptNum
        mymodel.expts.data_ms(i1).mdvs.val = myallMDVs(1:numbmass+1,:);
        mymodel.expts.data_ms(i1).mdvs.std = myallStds(1:numbmass+1,:);
        if sum(mymodel.expts.data_ms(i1).mdvs.val) == 0
            mymodel.expts.data_ms(i1).mdvs.on = false;
            mymodel.expts.data_ms(i1).on = false;
        else
            mymodel.expts.data_ms(i1).mdvs.on = true;
            mymodel.expts.data_ms(i1).on = true;            
        end
%    end
    if strcmp(Dec_INST,'Yes')
        mymodel.expts.data_ms(i1).mdvs.time = INSTtime;    
    end
    myallMDVs(1:numbmass+1,:)=[];
    myallStds(1:numbmass+1,:)=[];
end


%%%%%%%%%%%%%%%%%
% Adding flux data
%%%%%%%%%%%%%%%%%
% The flux data is assumed to be stored in the same *.xls as the metabolite
% MDVs. The user selects the fluxes and provides the associated reactions
% ids in the model manually.
Dec_FlxDat = questdlg('Do you want to load flux measurements?','Flux Measurements','Yes','No','Yes');
switch Dec_FlxDat
    case 'Yes'
        uiwait(msgbox('Select and copy the flux names in the following sheet view.','Select Flux Data'))
        uiimport(DataBucket.DataCorrected)
        uiwait(msgbox('Proceed when the relevant range is in the clipboard.','Data Selection'))
        myFlxNames=strsplit(clipboard('paste'),'\n')';
        uiwait(msgbox('Select and copy the flux values+std in the following sheet view.','Select Flux Data'))
        uiimport(DataBucket.DataCorrected)
        uiwait(msgbox('Proceed when the relevant range is in the clipboard.','Data Selection'))
        format long        
        myFlxData=str2num(clipboard('paste'));
        flxnum = size(myFlxData,1);
        FlxSeq = zeros(flxnum,1);
        for i1 = 1:flxnum
            FlxSeq(i1) = listdlg('PromptString',myFlxNames{i1},'SelectionMode','single','ListString',char(mymodel.rates.flx));
        end
        mymodel.expts.data_flx = data(mymodel.rates.flx(FlxSeq).id);
        for i1 = 1:flxnum
            mymodel.expts.data_flx(i1).val = myFlxData(i1,1);
            mymodel.expts.data_flx(i1).std = myFlxData(i1,2);
        end
end

%%%%%%%%%%%%%%%%%
% Adding pool sizes
%%%%%%%%%%%%%%%%%
% The pool size data is stored in the same *.xls as the metabolite
% MDVs. The metabolite IDs need to match the IDs in the model.
if strcmp(Dec_Pool,'Yes')
    [myPools,myNames,~] = xlsread(DataBucket.DataCorrected,'Pools','A:C');
    myNames(1,:) = [];
    Pool_Number = size(myPools,1);
    for i1 = 1:Pool_Number
        mymodel.expts.data_cxn(i1).val = myPools(i1,1);
        mymodel.expts.data_cxn(i1).std = myPools(i1,2);
        mymodel.expts.data_cxn(i1).id = {myNames(i1)};
    end
end


[DataPath,~,~] = fileparts(DataBucket.DataCorrected);
cd(DataPath)
save([date,'_INCA-model'],'mymodel')
msgbox('Data associated to INCA-model and stored as "[DATE]_INCA-model.mat".','Model Saved!')
end
