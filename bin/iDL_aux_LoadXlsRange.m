function numDat = iDL_aux_LoadXlsRange(FileName)
% Function for importing data via GUI from Excel to MATLAB.
%
% Input:
%        FileName: string, Excel data file
%
% Output:
%        numDat: array, numerical content of selected range in sheet

uiwait(msgbox('Select and copy data range in following interface.','Data Selection'))
uiimport(FileName)
uiwait(msgbox('Proceed when the relevant range is in the clipboard.','Data Selection'))
format long
numDat = str2num(clipboard('paste'));

end