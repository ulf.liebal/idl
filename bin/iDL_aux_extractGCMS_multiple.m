function GCMS_data = iDL_aux_extractGCMS_multiple(DataBucket)
% The function takes a folder-name as input and extracts MS-data for all
% data-types defined. The function 'iDL_aux_extractGCMS' is used to parse
% individual files. The output still contains absolute measured peak areas.
% iMS2Flux: https://doi.org/10.1186/1471-2105-13-295
% The final filename is 'date_iMS2Flux-input.txt'.
%
% Arguments: 
%    DataBucket: structure with subfields
%        DataSource: string, folder address
%        InputFileType: string, data-type ending (e.g. xls, dat, txt) to be
%        considered in the conversion
% 
% Output:
%    GCMS_data: structure with subfields
%          MeasMass: nx1 cell, contains identifier for each measured
%                    mass, e.g. 'ser_390'
%          Name: 1xn string, list of measured amino acids
%          Areas: nx1 string, list of peak areas
%          Data_Address: string, file location for iMS2Flux input
%            


myFileType = fullfile(DataBucket.DataSource,['*.',char(DataBucket.InputFileType)]);
myDataFiles = dir(myFileType);
FileNumber = size(myDataFiles,1);

% checking if there is a problem with loading different MS files. By
% default we assume everything is fine, but we still generate a cell for
% storing the eventual file names.
MSError = false; 
MSFileError_large = cell(FileNumber,1);
i2=1;
for i1 = 1:FileNumber
    myInput.DataSource = fullfile(DataBucket.DataSource,myDataFiles(i1).name);
    myInput.GenOutput = false;
    myInput.AnalyteClass = DataBucket.AnalyteClass;
%     myInput.iDL_Address = DataBucket.iDL_Address;
    myInput.QualityCheck = DataBucket.QualityCheck;
    try
        GCMS_Mult(i1) = iDL_aux_extractGCMS(myInput);
    catch 
        MSError = true;
        [~,FName,FType] = fileparts(myInput.DataSource);
        MSFileError_large{i2} = [FName,FType];
        sprintf('Problems reading MS file: %s',MSFileError_large{i2})
        MSFileIdx(i2) = i1;
        i2 = i2+1;
        continue
    end
end 

% providing user details on errors when reading specific MS-files.
if MSError
    % deleting preallocated storage for MS-error files
    GCMS_Mult(MSFileIdx) = [];
    % deleting file names from data file storage for output generation
    myDataFiles(MSFileIdx) = [];
    MsgName = 'MSFileConversionError.info';
    msgbox({'Errors occured during loading of MS-file measurements. Read following file in your data path for more information:';MsgName},'MS-File Error')
    MSFileError = MSFileError_large(~cellfun('isempty',MSFileError_large));
    fid = fopen([DataBucket.DataSource,filesep,MsgName],'w');
    fprintf(fid,'%s\n',MSFileError{:});
    fclose(fid)
end

% Final usable number of MS measurements
MmntNumber = size(GCMS_Mult,2);
% identifying all the AA that have been measured and checking whether all
% fragments are present
AATotal.Names = unique(cat(2,GCMS_Mult.Name))';
AATotal.MeasMass = unique(cat(1,GCMS_Mult.MeasMass));
AATotal.Areas = zeros(size(AATotal.MeasMass,1),MmntNumber);
% next we have to extract the specific masses measured for each fragment of
% the amino acids and put them to a total table
for i1 = 1:MmntNumber
    [~,MeasAA_idx] = intersect(AATotal.MeasMass,GCMS_Mult(i1).MeasMass);
    AATotal.Areas(MeasAA_idx,i1) = GCMS_Mult(i1).Areas;
end

GCMS_data.MeasMass = AATotal.MeasMass;
GCMS_data.Name = AATotal.Names;
GCMS_data.Areas = AATotal.Areas;
GCMS_data.AnalyteClass = DataBucket.AnalyteClass;

namecol = cell(size(AATotal.Areas,1),1);
mysplit = split(AATotal.MeasMass,':');
[AAmeasAll,AAmeasAll_idx] = unique(mysplit(:,1));
namecol(AAmeasAll_idx)=AAmeasAll;
masscol = str2num(char(mysplit(:,2)));
datacol = AATotal.Areas;
Output.Name = namecol;
Output.Mass = masscol;
Output.Area = datacol;    
T = struct2table(Output);
Tcell = table2cell(T);
ExpNames = split(string(char(myDataFiles.name)),'.');
Tl1 = [{'Response Values (Raw Data)','',cell(1,MmntNumber)}]; % here the amount of columns is defined. the measurements follow two lines of columns for AA-Name and Fragment Mass. r
Tl2 = [{'','',cellstr(ExpNames(:,1))'}];
Tnew = [Tl1;Tl2;Tcell];
T2 = cell2table(Tnew);
FileAddress = myDataFiles(1).folder;
GCMS_data.DataAddress = fullfile(char(FileAddress),['iMS2Flux-input.txt']);
writetable(T2,GCMS_data.DataAddress,'Delimiter','\t','WriteVariableNames',0)

end
