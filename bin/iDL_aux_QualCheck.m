function GCMS_data = iDL_aux_QualCheck(GCMS_data)
% This function corrects raw data according to quality checks on poor peak
% quality and threshold trespassing from iMS2Flux check output. Flagged
% fragments are set to NaN.
%
% Arguments:
%    GCMS_data: structure with subfields
%         MeasMass: cell, identities and masses of fragments.
%         Name: string array, unique names of fragments from original data
%         file.
%         Areas: string array. one dimensional table containing the areas
%         in string format
%         AnalyteClass: integer, measured metabolites, amino acids (1),
%                       soluble metabolites (2)
% Output:
%    GCMS_data: structure with subfields
%          MeasMass: nx1 cell, contains identifier for each measured
%                    mass, e.g. 'ser_390'
%          Name: 1xn string, list of measured amino acids
%          Areas: nx1 string, list of peak areas
%          Data_Address: string, file location for iMS2Flux input
%            (optional)

DataSource = GCMS_data.DataSoure;
[myAddress,~,~] = fileparts(DataSource);
% IMS2Flux quality check is stored in the following file
QCFile = 'iMS2Flux-input-Check.txt';

QCTab = readtable(fullfile(myAddress,QCFile));
% filling amino acid and metabolite names all over the first columns
% such that we find associated poor peak entries
QCTab = FillQCTabNames(QCTab);
% identify positions with questionable data
QCIdx = isnan(str2double(QCTab{:,3}));
% iMS2Flux reports questionable data after correction of M+numC+1, to
% disregard their output, we delete the associated raw data, which
% included M+numC+1

% For amino acids we have to identify the specific masses with bad
% quality and deactivate only those in the original measurement file
PoorPeakName = unique(QCTab{QCIdx,1}); 
Counter = 1:length(QCTab{:,1});
myFragDel = [];
for i1 = 1:length(PoorPeakName)        
    myCompIdx = Counter(contains(QCTab{:,1},PoorPeakName(i1)));
    DelMassInit = [1;find(QCTab{myCompIdx(2:end),2}-QCTab{myCompIdx(1:end-1),2}-1)+1];
    % identified PPQ for the selected fragment
    PPQIdent = isnan(str2double(QCTab{myCompIdx,3}));
    myLocalIdx = myCompIdx(PPQIdent);
    [~,~,myMassDet] = intersect(QCTab{myCompIdx(DelMassInit),2},QCTab{myLocalIdx,2});
    myMassDet = [myMassDet;length(myLocalIdx)+1];
    myList = [];
    for i2 = 1:length(myMassDet)-1
        MassBad = QCTab{myLocalIdx(myMassDet(i2):myMassDet(i2+1)-1),2};
        myList = [myList;MassBad;MassBad(end)+1];
    end
    myFragDel = [myFragDel;string(repmat(PoorPeakName(i1),length(myList),1)),myList];  
end

BadFragAll = cellstr(char(join(myFragDel,':')));
PoorPeakIdx = contains(GCMS_data.MeasMass,BadFragAll);
    
GCMS_data.Areas(PoorPeakIdx) = "NaN";
end

function QCTab = FillQCTabNames(QCTab)
% For Amino acids the iMS2Flux quality check output is too sparse. Some
% fragment masses are not associated to an amino acid name in the first
% column, complicating their identification. This problem is solved in this
% function in a way that each amino acid name is filled until the next
% amino acid starts.
    NamePosition = ~cellfun(@isempty, QCTab{:,1});
    NamePosIdx = [find(NamePosition);length(NamePosition)+1];
    for i1 = 1:sum(NamePosition)
        AAlength = NamePosIdx(i1):NamePosIdx(i1+1)-1;
        QCTab{AAlength,1} = repmat(QCTab{NamePosIdx(i1),1},length(AAlength),1);
    end
end
