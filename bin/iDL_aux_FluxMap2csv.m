function iDL_aux_FluxMap2csv(DataBucket)
% The function converts fitted flux maps from a metabolic flux analysis
% stored as matlab files by INCA into a comma separated file, with
% reactions as rows. It calls INCA functions, therefore INCA needs to be in
% the search path.
%
% Arguments: 
%    DataBucket: structure with subfields
%        DataSource: string, folder address
%        InputFileType: string, data-type ending (e.g. xls, dat, txt) to be
%        considered in the conversion
% 
% Output:
%    File: csv-format
%            

    % For the conversion we need INCA functions
    INCA_loaded=exist('model'); % test whether INCA is in the search path, result should be '6' as p-file.
    if INCA_loaded ~=6
        msgbox('Load INCA into Matlab search path.','INCA not found')
        return
    end

    % loading the flux map file with the MFA-fit, and renaming the variable
    % with the flux data.
    [FileName,FileDir,~]= uigetfile({'*.mat'},'Select Flux-Map File');
    INCA_Flux_mat = fieldnames(matfile(FileName));
    FitVariable = char(INCA_Flux_mat(2)); 
    FileInput = load(FileName); 
    FluxData = FileInput.(FitVariable);
    
    % Loading Model associated with MFA
    % The model is still saved in iDL
    if isfield(DataBucket, 'ModelStructure')
        mod = DataBucket.ModelStructure;
    elseif isfield(DataBucket, 'ModelSource') % the model is regenerated
        [ModelPath,ModelFile,~] = fileparts(DataBucket.ModelSource);
        cd(ModelPath)
        mod = feval(ModelFile);
    else
        msgbox('Model is missing, select "load existing Model".','Load Model')
        return
    end
    
    % Extracting model properties, within the subfield of P, K has the
    % number of reactions
    [P,~] = mod2mat(mod);
    
    % preparing data for net flux report, one directional
    Rct_idx = find(cellfun(@isempty, strfind(FluxData.par.id(1:length(P.K)), 'exch')));
    Flux_net = FluxData.par.val(Rct_idx)';
    Reaction_net = erase(FluxData.par.id(Rct_idx)', ' net');
    
    % Converting net to exchange fluxes
    Flux_fb = net2flx(FluxData.par.val(1:length(P.K))', P);
    Reaction_fb = mod.rates.flx.id'; 
    
    % generating table for export
    % net reactions without exchange
    T_net = table(Reaction_net, Flux_net);
    [~, myFileName, ~] = fileparts(FileName);
    CSV_Out = [FileDir, myFileName,'_flux-net.csv'];
    writetable(T_net, CSV_Out, 'Delimiter', ',')
    
    % explicit forward and backward reaction rates
    T_fb = table(Reaction_fb, Flux_fb);
    CSV_Out = [FileDir, myFileName,'_flux-abs.csv'];
    writetable(T_fb, CSV_Out, 'Delimiter', ',')
    msgbox('flux-csv stored.')
end