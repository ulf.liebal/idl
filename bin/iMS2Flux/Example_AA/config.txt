﻿//Data Related Options: 
MS Data File Name: Example_AA.txt
MS Data File Type: TSV
     MS Data Type: AA

Derivatizing Agent : TBDMS

Type of Data to Process [A|R]: R
Number of replicate data sets: 8 7 9
Replicate Column Header File : N
- Experimental titles (1/rep): 

//Additional Data Present: 
         M-1 Data Present: 0
    M+numC+1 Data Present: 1

//Data Checking Options:
Detector Limit Threshold: 0
     Poor Peak Threshold: 0
Retention Time Threshold: 0.0
   Optional RT Data File: N
     - RT Data File Name: 

    Data Value threshold: 0
Average Carbon Labelling: Y

//Perform corrections for:
Proton Loss (M-1) : N
Proton Gain (M+1) : N
Natural Abundance : Y
 Original Biomass : N
    - OB Data File: 

//Print options:
  Retention Time Matrix: N
Measurement Data Matrix: Y
  Processed Data Matrix: Y
  - Avg over replicates: Y
  - Standard deviations: Y

    Generate Model Data: Y
      - Generate Format: MS
      - In Model Files : N
          - Append Data: N
          - Model Files: 
//Other:
 S.D. Additive Constant: 0.0025