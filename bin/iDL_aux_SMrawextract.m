function AATable = iDL_aux_SMrawextract(myData,DataBucket)
% This function adjusts metabolite names according to the requirements of
% iMS2Flux.
% iMS2Flux: https://doi.org/10.1186/1471-2105-13-295
% Arguments:
%    myData: string array, three columns
%            1st column: metabolite name from raw file
%            2nd column: mass
%            3rd column: Area/Peak
%    fctinput: structure with subfields, 
%         iDL_Address: string, iDL address on computer in order to load 
%              iMS2Flux soluble metabolite reference list
%         QualityCheck: boolean, determines whether a previous quality
%              check output of iMS2Flux is used to ignore bad quality data
% Output:
%    AATable: structure with subfields
%          MeasMass: nx1 cell, contains identifier for each measured
%                    mass, e.g. 'ser_390'
%          Name: 1xn string, list of measured amino acids
%          Areas: nx1 string, list of peak areas

iDL_Address = DataBucket.iDL_Address;

% loading a list with the abbreviations
iDL_aux_abrTable
% loading iMS2Flux reference soluble metabolite list
RefList_file = fullfile(iDL_Address,'bin','SM_Data_Table.txt');
SM_RefList = readtable(RefList_file);


% replacing the full name by the abbreviation
myAAuniquename = unique(myData(:,1));
AAreplace = cell(size(myData,1),1);
for i1 = 1:size(myAAuniquename,1)
    % finding the metabolite in the raw list
    myAbr = find(strcmpi(myAAuniquename(i1),abrTable));
    AAreplaceIdx = find(strcmpi(myData(:,1),myAAuniquename(i1,1)));
    
    % finding fragment details in the reference list
    SM_LstIx = find(contains(string(SM_RefList{:,1}),string(abrTable(myAbr,2)))); % number and index of fragments
    for i2 = 1:length(SM_LstIx)
        % calculating fragment length
        CarbBack = str2double(strsplit(string(SM_RefList{SM_LstIx(i2),11}),'-'));
        CarbNumb = CarbBack(2)-CarbBack(1)+3; % +3, for M+0, and additional mass measurement
        % finding the mass of the relevant fragment in the reference list
        FragStart = find(str2double(myData(AAreplaceIdx,2))==SM_RefList{SM_LstIx(i2),10});
        AAreplace(AAreplaceIdx(FragStart:FragStart+CarbNumb-1)) = SM_RefList{SM_LstIx(i2),1};    
        
    end
end

% Only exporting data which is part of the iMS2Flux SM reference list. 
% Data points that are part of the list have entries in 'AAreplace'.
RefPosData = find(~cellfun(@isempty,AAreplace));
myAAuniquename_new = unique(myData(RefPosData,1));

AATable.MeasMass = regexprep(cellstr([char(AAreplace(RefPosData)),repmat(':',size(RefPosData,1),1),char(myData(RefPosData,2))]),' ','');
AATable.Name = myAAuniquename_new';
AATable.Areas = myData(RefPosData,3);
end