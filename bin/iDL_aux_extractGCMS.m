function [GCMS_data] = iDL_aux_extractGCMS(DataBucket)
% The function converts GCMS measurements coming from Termo-X-Report in
% XCalibur into the format used by iMS2Flux. The output still contains 
% absolute measured peak areas.
% iMS2Flux: https://doi.org/10.1186/1471-2105-13-295
% The final filename is 'date_iMS2Flux-input.txt'.
%
% Input:
%    DataBucket: structure with subfields
%         datafile: string, adress of the raw file.
%         iMS2Flux-filename: string, adress and name of the output txt file
%         GenOutput: boolean, decides the creation of an output file.
%         AnalyteClass: integer, measured metabolites, amino acids (1),
%                       soluble metabolites (2)
%         iDL_Address: string, iDL address 
%         QualityCheck: boolean, determines whether a previous quality
%                       check output of iMS2Flux is used to ignore bad
%                       quality data
% Output:
%    GCMS_data: structure with subfields
%          MeasMass: nx1 cell, contains identifier for each measured
%                    mass, e.g. 'ser_390'
%          Name: 1xn string, list of measured amino acids
%          Areas: nx1 string, list of peak areas
%          Data_Address: string, file location for iMS2Flux input
%            (optional)

DataSource = DataBucket.DataSource;
GenOutput = DataBucket.GenOutput;
AnalyteClass = DataBucket.AnalyteClass;
% iDL_Address = DataBucket.iDL_Address;
QualityCheck = DataBucket.QualityCheck;

[~,Ttxt,~]=xlsread(DataSource);

% extracting the retention time
% retention times start with rows below 'RT', several of these can occur
datarows = size(Ttxt);
mytmp = strfind(Ttxt(:,1),'RT');
% indices where 'RT' is written in the columns
myRTidx = find(~cellfun(@isempty,mytmp));
RowRTAdj = setdiff(myRTidx(1):datarows,myRTidx);
RowBlAdj = setdiff(RowRTAdj,find(cellfun(@isempty,Ttxt(:,1))));
% setting N/A values to zero
RowNA = contains(Ttxt(RowBlAdj,1),'N/A');

% removing of N/A values
% RowNAAdj = setdiff(RowBlAdj,find(~cellfun(@isempty,strfind(Ttxt(:,1),'N/A'))));
% myRT = str2num(char(Ttxt(RowBlAdj,1)));

%% extracting coherent AA list
myAAlist = string(Ttxt(RowBlAdj,2));
myAAlistsplit = split(myAAlist,'_');
% extracting area measurements
% finding Area column
[~,Area_colAll] = find(contains(Ttxt,'Area'));
Area_col = unique(Area_colAll);
if length(Area_col)>1
    msgbox('MDV areas are stored in different columns of data source, restrict to one column only.');
    return
end

% setting measured areas of N/A values to zero
Ttxt(RowBlAdj(RowNA),Area_col) = {'0'};
myAreas=str2num(char(Ttxt(RowBlAdj,Area_col)));

% all data sorted
myData = sortrows([myAAlistsplit,myAreas]);
% floor-rounding masses to integers
myData(:,2) = string(floor(str2num(char(myData(:,2)))));

switch AnalyteClass
    case 1 % Analysis for Amino acids
        GCMS_data = iDL_aux_AArawextract(myData);
        
    case 2 % Analysis for soluble metabolites
        GCMS_data = iDL_aux_SMrawextract(myData,DataBucket);
end
GCMS_data.AnalyteClass = AnalyteClass;
% We need to sort the table, because later we extract unique features
[~,SortIx] = sort(GCMS_data.MeasMass);
GCMS_data.MeasMass = GCMS_data.MeasMass(SortIx);
GCMS_data.Areas = GCMS_data.Areas(SortIx);
GCMS_data.DataSoure = DataSource;

%% performing quality control based on iMS2Flux analysis of peak quality 
%  and thresholds
% IMS2Flux quality check is stored in the following file
QCFile = 'iMS2Flux-input-Check.txt';

% The quality check is always done right before saving the output to a
% file. That means for single file input the quality checks are performed
% here, whereas when xls in a folder are analysed the quality check is
% performed in 'iDL_aux_extractGCMS_multiple.m'
if QualityCheck && isfile(QCFile) %&& GenOutput
    GCMS_data = iDL_aux_QualCheck(GCMS_data);
    disp('Performing quality corrections')
end


%% converting back to a column
switch GenOutput
    case true
        namecol = cell(size(GCMS_data.Areas,1),1);
        mysplit = split(GCMS_data.MeasMass,':');
        [AAmeasAll,AAmeasAll_idx] = unique(mysplit(:,1));
        namecol(AAmeasAll_idx)=AAmeasAll;
        masscol = str2num(char(mysplit(:,2)));
        datacol = GCMS_data.Areas;
        Output.Name = namecol;
        Output.Mass = masscol;
        Output.Area = str2num(char(datacol));  
   

        T = struct2table(Output);
        Tcell = table2cell(T);
        Tnew = [{'Response Values (Raw Data)','','';'','','Exp1'};Tcell];
        T2 = cell2table(Tnew);
        [FileAddress,FileName] = fileparts(DataSource);
        GCMS_data.DataAddress = fullfile(char(FileAddress),['iMS2Flux-input.txt']);
        writetable(T2,GCMS_data.DataAddress,'Delimiter','\t','WriteVariableNames',0)
end
