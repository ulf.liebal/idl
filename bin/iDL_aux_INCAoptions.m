function mymodel = INCA_options(mymodel)
%% INCA Options for MFA simulations
mymodel.options.sim_tunit = 'h';              % hours are unit of time
mymodel.options.fit_reinit = true; 
mymodel.options.fit_starts = 20; 
mymodel.options.fit_nudge = 4; 
mymodel.options.sim_ss = true; 
mymodel.options.sim_sens = false;
mymodel.options.sim_more = false; % natural abbundance of labeled Cs
mymodel.options.sim_na = false; % natural abbundance of non-labeled Cs
mymodel.options.int_timeout=1000;
mymodel.options.hpc_on=true;
mymodel.options.hpc_bg=false;
end
