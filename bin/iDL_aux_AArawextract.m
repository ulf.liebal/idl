function AATable = iDL_aux_AArawextract(myData)
% This function adjusts metabolite names according to the requirements of
% iMS2Flux.
% iMS2Flux: https://doi.org/10.1186/1471-2105-13-295
% Arguments:
%    myData: string array, three columns
%            1st column: metabolite name from raw file
%            2nd column: mass
%            3rd column: Area/Peak
% Output:
%    AATable: structure with subfields
%          MeasMass: nx1 cell, contains identifier for each measured
%                    mass with amino acid identifier, e.g. 'ser_390'
%          Name: 1xn string, list of measured amino acids
%          Areas: nx1 string, list of peak areas

% loading a list with the abbreviations
iDL_aux_abrTable
% identifying index position in which amino acid names are not already
% abbreviated, or do not contain standard name from abrTable
AA_FullName_idx = find(~contains(lower(myData(:,1)),abrTable(:,2)));
% we assume that if we fail finding abbreviated names, they are by default
% written in full form
if logical(sum(AA_FullName_idx)) % =1 if AA_FullName_idx is empty, i.e., names are long
    % deleting substances not contained in the abrTable
    Sub_unident = find(~contains(lower(myData(:,1)),lower(abrTable(:,1))));
    myData(Sub_unident,:) = [];
    % replacing the full name by the abbreviation
    myAAuniquename = unique(lower(myData(:,1)));
    AAreplace = cell(size(myData,1),1);
    for i1 = 1:size(myAAuniquename,1)
        myAbr = find(strcmpi(myAAuniquename(i1),abrTable));
        AAreplaceIdx = find(strcmpi(myData(:,1),myAAuniquename(i1,1)));
        AAreplace(AAreplaceIdx) = abrTable(myAbr,2);
    end
else
    myAAuniquename = unique(lower(myData(:,1)));
    AAreplace = cell(size(myData,1),1);
    for i1 = 1:size(myAAuniquename,1)
        myAbr = find(strcmpi(myAAuniquename(i1),abrTable));
        AAreplaceIdx = find(strcmpi(myData(:,1),myAAuniquename(i1,1)));
        AAreplace(AAreplaceIdx) = abrTable(myAbr,2); % cellstr(myAAuniquename{i1});
    end
    
end


AATable.MeasMass = regexprep(cellstr([char(AAreplace),repmat(':',size(AAreplace,1),1),char(myData(:,2))]),' ','');
AATable.Name = myAAuniquename';
AATable.Areas = myData(:,3);
end