function corrGCMS = iDL_aux_iMS2Flux(GCMS_data)
% The function performs corrections on GCMS data according to the iMS2Flux
% routine.
% iMS2Flux: https://doi.org/10.1186/1471-2105-13-295
% The final filename is 'date_iMS2Flux-output.txt'.


    iDL_Address = fullfile(GCMS_data.iDL_Address,'template');
    Target_Address = fileparts(GCMS_data.DataAddress);
    cd(Target_Address)
    iconfig = dir('config.txt');
    if ~sum(size(iconfig,1))
        Decision_config = questdlg('Copy template config.txt and original biomass file (OBM) to data directory?','Config-file decision','No','Both','config only','No');
    else
        Decision_config = false;
    end
    % copying the right config file based on analyte class
    switch GCMS_data.AnalyteClass
        case 1
            config_name = 'config_AA.txt';
        case 2                
            config_name = 'config_SM.txt';
    end

    switch Decision_config
        case 'config only'
            config_source = fullfile(iDL_Address,config_name);
            copyfile(config_source,fullfile(Target_Address,'config.txt'))
        case 'Both'
            Target_Address = fileparts(GCMS_data.DataAddress);
            config_source = fullfile(iDL_Address,config_name);
            copyfile(config_source,fullfile(Target_Address,'config.txt'))
            OBM_source = fullfile(iDL_Address,'OBM.txt');
            copyfile(OBM_source,Target_Address)
    end


    uiwait(msgbox({'Adapt the "config.txt" to your needs and proceed when finished.'}));

    % provide iMS2Flux address
    [FileName,FilePath] = uigetfile('*.pl','iMS2Flux Address','iMS2Flux.pl');
    [~,status] = perl(fullfile(FilePath,FileName),'config.txt');
    disp('iMS2Flux output status:')
    disp(status)
    
    % if the config.txt contains quality check points for poor peak quality
    % and threshold values then the status will have different values
    corrGCMS =1;
    switch status
        case 0
            msgbox({'iMS2Flux output has been stored in: ';Target_Address});
        case 255
            msgbox({'iMS2Flux Quality check has been stored in: ';Target_Address;'Perform iMS2Flux conversion to disregard poor data'});
        case -1
            msgbox({'iMS2Flux Quality check has been stored in: ';Target_Address;'Perform iMS2Flux conversion to disregard poor data'});
        otherwise    
            errordlg({'There has been an unrecognized error during the iMS2Flux processing'});
    end
end