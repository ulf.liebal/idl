function iDL_main
% GUI for conversion of GC-MS data into an INCA model.

% Create canvas
canvdim = [360,500,750,250]; % canvas dimension
f = figure('NumberTitle','off','Visible','off','Position',canvdim);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Construct the components of GUI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GC/MS rw data load and iMS2Flux conversion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
panel_DataCorrection = uipanel('Title','Data Correction','FontSize',12,'BackgroundColor','white','Position',[.02,.05,.3,.9]);
%
% calculating positions of buttons
%
% 1. general button rows
buttonrow = 3; % number of rows, including the popup menu
buttoncol = 2; % number of button columns
buttonnum = 4; % number of buttons
buttonsp = [.05,.03]; % button distance to top and bottom of panel, free space between buttons
buttondim = [.9,(1-2*buttonsp(1)-(buttonrow-1)*buttonsp(2))/buttonrow]; % x,y-dimension of buttons
buttonpos = zeros(buttonrow,buttonnum);
buttonpos(:,1) = buttonsp(1);
buttonpos(:,3) = buttondim(1);
buttonpos(:,4) = buttondim(2);
for i1 = 1:buttonrow
    buttonpos(i1,2) = buttonsp(1)+(i1-1)*(buttondim(2)+buttonsp(2));
end
% subdividing button rows to contain two buttons
butleftdown = buttonpos(1,:); butleftdown(3) = .4;
butleftup = buttonpos(2,:); butleftup(3) = .4;
butrightdown = butleftdown; butrightdown(1) = .55;
butrightup = butleftup; butrightup(1) = .55;

%
% putting buttons to place
%
uicontrol('Parent',panel_DataCorrection,'Style','popup','String',{'Amino Acids MDVs','Metabolite MDVs'},'Callback',@DatCor_AnalyteClass,'Units','normalized','Position',buttonpos(3,:));
uicontrol('Parent',panel_DataCorrection,'Style','pushbutton','String','load file','Callback',@DatCor_LoadFile,'Units','normalized','Position',butleftup);
uicontrol('Parent',panel_DataCorrection,'Style','pushbutton','String','load folder','Callback',@DatCor_LoadFolder,'Tag','UserAddress','Units','normalized','Position',butrightup);
uicontrol('Parent',panel_DataCorrection,'Style','pushbutton','String','iMS2Flux','Callback',@DatCor_iMS2Flux,'Units','normalized','Position',butrightdown);
uicontrol('Parent',panel_DataCorrection,'Style','pushbutton','String','Qual.-Check','Callback',@DatCor_RawQualCheck,'Units','normalized','Position',butleftdown);
% htext  = uicontrol('Style','text','String',hGCMS.Value,'Units','normalized','Position',[325,90,60,15]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data-Model connection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
panel_DataCorrection = uipanel('Title','Data-Model Connection','FontSize',12,'BackgroundColor','white','Position',[.35,.05,.3,.9]); % [.02,.2,.3,.33]
%
% calculating positions of buttons
%
buttonnr = 3;
buttonsp = [.05,.03]; % button distance to top and bottom of panel, free space between buttons
buttondim = [.9,(1-2*buttonsp(1)-(buttonnr-1)*buttonsp(2))/buttonnr]; % x,y-dimension of buttons
buttonpos = zeros(buttonnr,4);
buttonpos(:,1) = buttonsp(1);
buttonpos(:,3) = buttondim(1);
buttonpos(:,4) = buttondim(2);
for i1 = 1:buttonnr
    buttonpos(i1,2) = buttonsp(1)+(i1-1)*(buttondim(2)+buttonsp(2));
end
%
% putting buttons to place
%
uicontrol('Parent',panel_DataCorrection,'Style','pushbutton','String','select Data','Callback',@DatMod_SelDat,'Units','normalized','Position',buttonpos(3,:));
uicontrol('Parent',panel_DataCorrection,'Style','pushbutton','String','select Model','Callback',@DatMod_SelMod,'Tag','UserAddress','Units','normalized','Position',buttonpos(2,:));
uicontrol('Parent',panel_DataCorrection,'Style','pushbutton','String','save Model','Callback',@DatMod_SaveDatMod,'Units','normalized','Position',buttonpos(1,:));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Flux Output conversion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
panel_DataCorrection = uipanel('Title','Flux Output conversion','FontSize',12,'BackgroundColor','white','Position',[.68,.35,.3,.6]); % [.02,.2,.3,.33]
%
% calculating positions of buttons
%
buttonnr = 2;
buttonsp = [.05,.03]; % button distance to top and bottom of panel, free space between buttons
buttondim = [.9,(1-2*buttonsp(1)-(buttonnr-1)*buttonsp(2))/buttonnr]; % x,y-dimension of buttons
buttonpos = zeros(buttonnr,4);
buttonpos(:,1) = buttonsp(1);
buttonpos(:,3) = buttondim(1);
buttonpos(:,4) = buttondim(2);
for i1 = 1:buttonnr
    buttonpos(i1,2) = buttonsp(1)+(i1-1)*(buttondim(2)+buttonsp(2));
end
% % %
% % putting buttons to place
% %
uicontrol('Parent',panel_DataCorrection,'Style','pushbutton','String','generate Flux-csv','Callback',@FluxOut_GenCSV,'Units','normalized','Position',buttonpos(1,:));
uicontrol('Parent',panel_DataCorrection,'Style','pushbutton','String','load existing Model','Callback',@FluxOut_loadModel,'Tag','UserAddress','Units','normalized','Position',buttonpos(2,:));
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Exit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h_exit = uicontrol('Style','pushbutton','String','Exit','Callback',@exit_Callback,'Tag','UserAddress','Units','normalized','Position',[.68,.05,.3,.25]);

iDL_Address = fileparts(which('iDL_main.m'));
addpath(genpath(iDL_Address))
DataBucket = guihandles(f);
DataBucket.iDL_Address = iDL_Address;
DataBucket.WhichButton = 0;
DataBucket.AnalyteClass = 1;
DataBucket.InputFileType = 'xls';
DataBucket.GenOutput = false;
DataBucket.QualityCheck = false;
guidata(f,DataBucket);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition of callback functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions for data correction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function DatCor_AnalyteClass(hObject, ~, ~)
        AnalyteClass = get(hObject,'Value');
        DataBucket = guidata(hObject);
        DataBucket.AnalyteClass = AnalyteClass;
        guidata(hObject,DataBucket);
    end

    function DatCor_LoadFile(hObject, ~, ~)
        [FileName,FilePath]= uigetfile({'*.xls';'*.xlsx';'*.txt';'*.csv';'*.dat'});
        ExPath = fullfile(FilePath, FileName);
        DataBucket = guidata(hObject);
        DataBucket.DataSource = ExPath;
        DataBucket.WhichButton = 1;
        DataBucket.GenOutput = true;
        guidata(hObject,DataBucket);
    end

    function DatCor_LoadFolder(hObject, ~, ~)
        myFileType = inputdlg('Enter Input File Type','Input',[1],{'xls'});
        ExPath= uigetdir();
        hObject.UserData = ExPath;
        DataBucket = guidata(hObject);
        DataBucket.InputFileType = myFileType;
        DataBucket.DataSource = ExPath;
        DataBucket.WhichButton = 2;
        guidata(hObject,DataBucket);
        
%         % due to the export of data with table, multiple GC-MS measurement
%         % lead to nested cells, that in turn result in hyphenation in the
%         % final txt export. In the following I reload the file and delete
%         % hyphenation
        fid  = fopen('iMS2Flux-input.txt','rt');
        f=fread(fid);
        fclose(fid);
        f = strrep(f,'"','');
        fid  = fopen('iMS2Flux-input.txt','w');
        fprintf(fid,'%s',f);
        fclose(fid);

    end

    function DatCor_RawQualCheck(hObject, ~, ~)
        DataBucket = guidata(hObject);
        DataBucket.QualityCheck = true;
        guidata(hObject,DataBucket);
        DatCor_iMS2Flux(hObject, [], [])
    end

    function DatCor_iMS2Flux(hObject, ~, ~)
        DataBucket = guidata(hObject);
        disp(DataBucket.AnalyteClass)
        WhichButton = DataBucket.WhichButton;
        switch WhichButton
            case 1
                GCMS_data = iDL_aux_extractGCMS(DataBucket);
                GCMS_data.iDL_Address = DataBucket.iDL_Address;
                uiwait(msgbox({'iMS2Flux input stored as: ';GCMS_data.DataAddress}));
                disp(['Stored as: ',GCMS_data.DataAddress])
            case 2
                GCMS_data = iDL_aux_extractGCMS_multiple(DataBucket);
                GCMS_data.iDL_Address = DataBucket.iDL_Address;
                uiwait(msgbox({'iMS2Flux input stored as: ';GCMS_data.DataAddress}));
                disp(['Stored as: ',GCMS_data.DataAddress])
            otherwise
                myMsg = msgbox('choose data first');
        end
        % running iMS2Flux
%                GCMS_data.QualityCheck = DataAddress.QualityCheck;

        iDL_aux_iMS2Flux(GCMS_data);
    end

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions for data-model connection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function DatMod_SelDat(hObject, ~, ~)
        DataBucket = guidata(hObject);
        File_Stdev = questdlg('Load separate file for standard deviation?','Standard Deviation Source','No','Yes','Generate synthetic StdDev','No');
        switch File_Stdev
            case 'No'             
                [FileName,FilePath]= uigetfile({'*.txt;*.csv;*.dat;*.xlsx;*.xls';'*.*'},'Select Data File');
                ExPath = fullfile(FilePath, FileName);
                DataBucket.DataCorrected = ExPath;        
                DataBucket.DataStandardDev = ExPath;        
            case 'Yes'             
                [FileName,FilePath]= uigetfile({'*.txt;*.csv;*.dat;*.xls;*.xlxs;*.*'},'Select Average MDV');
                AvrgPath = fullfile(FilePath, FileName);
                DataBucket.DataCorrected = AvrgPath;
                [FileName,FilePath]= uigetfile({'*.txt;*.csv;*.dat;*.xlsx;*.xls';'*.*'},'Select Standard Deviation');
                StdPath = fullfile(FilePath, FileName);
                DataBucket.DataStandardDev = StdPath;        
            case 'Generate synthetic StdDev'
                [FileName,FilePath]= uigetfile({'*.txt;*.csv;*.dat;*.xlsx;*.xls';'*.*'},'Select Data File');
                ExPath = fullfile(FilePath, FileName);
                DataBucket.DataCorrected = ExPath;        
                DataBucket.DataStandardDev = 0.05; % default standard deviation for MDV is 5%
        end
        guidata(hObject,DataBucket);
    end

    function DatMod_SelMod(hObject, ~, ~)
        [FileName,FilePath]= uigetfile('*.m','Select Model');
        ExPath = fullfile(FilePath, FileName);
        DataBucket = guidata(hObject);
        DataBucket.ModelSource = ExPath;
        guidata(hObject,DataBucket);
    end

    function DatMod_SaveDatMod(hObject, ~, ~)
        DataBucket = guidata(hObject);        
%        iDL_aux_Dat2Mod(DataBucket)
        mod = iDL_aux_Dat2Mod(DataBucket);
        DataBucket.ModelStructure = mod;
        guidata(hObject,DataBucket);
    end


%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions for data export
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function FluxOut_loadModel(hObject, ~, ~)
        DataBucket = guidata(hObject);
        % loading the existing model file, and renaming the model variable
        [FileName,~,~]= uigetfile({'*.mat'},'Select Model File');
        INCA_Flux_mat = fieldnames(matfile(FileName));
        ModVariable = char(INCA_Flux_mat(2)); 
        FileInput = load(FileName); 
        mod = FileInput.(ModVariable);
        DataBucket.ModelStructure = mod;
        guidata(hObject,DataBucket);        
    end

    function FluxOut_GenCSV(hObject, ~, ~)
        DataBucket = guidata(hObject);
        iDL_aux_FluxMap2csv(DataBucket)
    end


%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function exit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function exit_Callback(~, ~, ~)
        delete(f)
    end


% Assign a name to appear in the window title.
f.Name = 'iDL - INCA-DATA-LOADER';
% Move the window to the center of the screen.
movegui(f,'center')
% Make UI visibĺe
f.Visible = 'on';


end